"use strict";

// TODO:
/*
+0 Створити різні папочки по номерам модулів, в одній репо на Gitlab
+1 Створити додавачку з інпута і двох кнопок 
2 Створити slag як приклад URL(використовувати chain для методів) 
"freedom-of-speech-not-reach-new-updates-and-progress" 
"Freedom-of-speech-not-reach-new-updates-and-PROGRESS"
3 написати свої методи додавання віднімання, пошуку і т. д. додати їх в інтерфейс
4 Поклацати методи масивів
)
*/

const controlsRef = {
  inputRef: document.querySelector("[name=number-input]"),
  addBtnRef: document.querySelector("[data-js=addBtn]"),
  calculateBtnRef: document.querySelector("[data-js=calculateBtn]"),
  headingInputRef: document.querySelector("[name=heading-input]"),
  slagMakeBtn: document.querySelector("[data-js=makeSlagBtn]"),
};

const userInputArray = [];
let sumOfArray = 0;

controlsRef.addBtnRef.addEventListener("click", () => {
  userInputArray.push(Number(controlsRef.inputRef.value));
  controlsRef.inputRef.value = "";
  console.log(userInputArray);
});

controlsRef.calculateBtnRef.addEventListener("click", () => {
  for (const el of userInputArray) {
    sumOfArray += el;
  }
  console.log(sumOfArray);
});

controlsRef.slagMakeBtn.addEventListener("click", () => {
  let slag = "";
  let userHeading = controlsRef.headingInputRef.value;
  const userSlag = userHeading.toLowerCase().split(" ");
  // slag = userSlag.join("-"); BEST Variant
  for (let i = 0; i < userSlag.length; i++) {
    const isLastWord = i === userSlag.length - 1;
    slag += isLastWord ? userSlag[i] : userSlag[i] + "-";
  }
  controlsRef.headingInputRef.value = "";
  console.log(slag);
});

// --------

const userArray = ["a", "b", "c", "d", "e", "f"];

function arrLogger(arr) {
  console.log(arr, `length: ${arr.length}`);
}
arrLogger(userArray);

userArray[-1] = "x";
userArray[-5] = "z";
arrLogger(userArray);

for (const iterator of userArray) {
  if (iterator === "c") {
    continue;
  }
  console.log(iterator);
}
