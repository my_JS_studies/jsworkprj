"use strict";
console.log("functions");
const testArrowFn = (el) => el * 2;

console.log(testArrowFn(33));

const barAssortment = ["🍺", "🍷", "🍾", "🍸", "🍹"];
const options = ["🧊"];

const makeClientScope = (name) => {
  const clientScope = [];
  const clientName = name;
  return { clientName, clientScope };
};

const showDrinks = ({ clientScope }) => {
  console.log(...clientScope);
};
const addOptionToDrink = (drink, option) => `${drink} with ${option}`;

const addDrinkToClientScope = (drink, client) => {
  client.clientScope.push(drink);
};

const clientDima = makeClientScope("Dima");
addDrinkToClientScope(
  addOptionToDrink(barAssortment[2], options[0]),
  clientDima
);

addDrinkToClientScope(
  addOptionToDrink(barAssortment[1], options[0]),
  clientDima
);
console.log(clientDima);
