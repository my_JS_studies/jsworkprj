'use strict';
export const shoppingCardWithDestr = {
  items: [],
  getItems() {
    return this.items;
  },
  addItem(product) {
    for (const item of this.items) {
      if (item.ID === product.ID) {
        item.quantity++;
        return;
      }
    }
    this.items.push({ ...product, quantity: 1 });
  },
  removeItem({ ID }) {
    // const items = this.items;
    const { items } = this;
    for (let i = 0; i < items.length; i++) {
      const { itemID = ID } = items[i];
      if (ID === itemID) items.splice(i, 1);
      return;
    }
  },
  clearShoppingCard() {
    this.items = [];
  },
  countTotalPrice() {
    let total = 0;
    for (const { price, quantity } of this.items) {
      total += price * quantity;
    }
    return total;
  },
  decreaseQuantity(product) {
    for (const item of this.items) {
      if (item.ID === product.ID) {
        item.quantity === 1 ? this.removeItem(product) : item.quantity--;
        return;
      }
    }
  },
  increaseQuantity(product) {
    for (const item of this.items) {
      if (item.ID === product.ID) {
        item.quantity++;
      }
    }
  },
};
