'use strict';
const run = function () {
  console.log('hello from run');
};

const machine = {
  name: 'lathe',
  power: 24,
  tools: {
    cutters: 'left',
    // rollers: 1000,
    drills: true,
  },
};
// run();

const {
  name,
  power,
  tools: { cutters, rollers: myRollers = 600, drills },
} = machine;

// console.log(myRollers);

const colors = [255, 100, 90];

const [red, green, blue, ...last] = colors;

// console.log(red, last);

const testFn = function (age, username) {
  console.log(age, username);
};
// testFn('Dima');
function makeTask(data) {
  const completed = false;
  const category = 'General';
  const priority = 'Normal';
  // Change code below this line
  return {
    ...data,
  };
  // Change code above this line
}
console.log(makeTask({}));
console.log(makeTask({ category: 'Homemade', priority: 'Low', text: 'Take out the trash' }));
console.log(makeTask({ category: 'Homemade', priority: 'Low', text: 'Take out the trash' }));
console.log(makeTask({ category: 'Finance', text: 'Take interest' }));

// const atTheOldToad = {
//   potions: ['Speed potion', 'Dragon breath', 'Stone skin'],
//   removePotion(potionName) {
//     // Change code below this line
//     const potions = this.potions;
//     potions.splice(potions.indexOf(potionName), 1);

//     // Change code above this line
//   },
// };
// atTheOldToad.removePotion('Dragon breath');
// console.log(atTheOldToad.potions);

const atTheOldToad = {
  potions: [
    { name: 'Speed potion', price: 460 },
    { name: 'Dragon breath', price: 780 },
    { name: 'Stone skin', price: 520 },
  ],
  // Change code below this line
  getPotions() {
    return this.potions;
  },
  addPotion(newPotion) {
    for (let potion of this.potions) {
      if (potion.name === newPotion.name) {
        return `Error! Potion ${newPotion.name} is already in your inventory!`;
      }
      this.potions.push(newPotion);
    }
  },
  removePotion(potionName) {
    for (let i = 0; i < this.potions.length; i++) {
      if (this.potions[i].name === potionName) {
        this.potions.splice(i, 1);
        return;
      }
      // return `Potion ${potionName} is not in inventory!`;
    }
  },
  updatePotionName(oldName, newName) {
    for (let potion of this.potions) {
      if (potion.name === oldName) {
        return (potion.name = newName);
      }
    }
  },
  // Change code above this line
};

atTheOldToad.addPotion({ name: 'Dragon breath', price: 700 });

console.log(atTheOldToad.potions);
