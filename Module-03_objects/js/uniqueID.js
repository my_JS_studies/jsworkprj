"use strict";
const lettersStr = "ABSDIFGHIJKLMNOPQRSTUVWXWZ";
export const generateID = (lengthOfID = 7) => {
  let ID = "I";
  for (let i = 0; i <= lengthOfID; i++) {
    if (i % 3 == 0) ID += "-";
    ID += lettersStr[Math.floor(Math.random() * lettersStr.length)];
  }
  ID += `-${Math.floor(Math.random() * 100)}`;
  return ID;
};
