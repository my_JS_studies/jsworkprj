'use strict';

export const shoppingCart = {
  items: [],
  getItems() {
    console.table(this.items);
  },
  addItem(product) {
    for (const item of this.items) {
      if (item.ID === product.ID) {
        item.quantity++;
        return;
      }
    }
    this.items.push({ ...product, ...{ quantity: 1 } });
  },
  removeItem(product) {
    for (let i = 0; i < this.items.length; i++) {
      if (product.ID === this.items[i].ID) {
        this.items.splice(i, 1);
        return;
      }
    }
  },
  clearShoppingCart() {
    this.items = [];
  },
  countTotalPrice() {
    let total = 0;
    for (const item of this.items) {
      total += item.quantity * item.price;
    }
    return total;
  },
  decreaseQuantity(product) {
    for (let i = 0; i < this.items.length; i++) {
      if (product.ID === this.items[i].ID) {
        if (this.items[i].quantity === 1) {
          this.removeItem(product);
          return;
        }
        this.items[i].quantity--;
        return;
      }
    }
  },
  increaseQuantity(product) {
    for (let i = 0; i < this.items.length; i++) {
      if (product.ID === this.items[i].ID) {
        this.items[i].quantity++;
        return;
      }
    }
  },
};

/* TODO:
Create methods for shopping cart with destructuration .
*/
