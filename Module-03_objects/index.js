'use strict';
import { shoppingCart } from './js/shoppingCart.js';
import { generateID } from './js/uniqueID.js';
import { shoppingCardWithDestr } from './js/shoppingCardWithDestr.js';
// import { run } from './js/spreadRest.js';

const assortment = [
  { name: 'beer', price: 35, ico: '🍺', ID: generateID() },
  { name: 'vine', price: 180, ico: '🍷', ID: generateID() },
  { name: 'champaign', price: 54, ico: '🍾', ID: generateID() },
  { name: 'martini', price: 231, ico: '🍸', ID: generateID() },
  { name: 'juice', price: 22, ico: '🍹', ID: generateID() },
];
// shoppingCart calls
/*
{// shoppingCart.addItem(assortment[0]);
// shoppingCart.getItems();
// shoppingCart.addItem(assortment[0]);
// shoppingCart.clearShoppingCart();
// shoppingCart.addItem(assortment[2]);
// shoppingCart.addItem(assortment[2]);
// shoppingCart.addItem(assortment[2]);
// shoppingCart.addItem(assortment[0]);
// shoppingCart.addItem(assortment[0]);
// shoppingCart.removeItem(assortment[0]);

// console.log(shoppingCart.countTotalPrice());
// console.log(shoppingCart.items);

// shoppingCart.addItem(assortment[4]);
// shoppingCart.addItem(assortment[4]);
// shoppingCart.addItem(assortment[4]);
// shoppingCart.addItem(assortment[4]);
// shoppingCart.getItems();
// shoppingCart.decreaseQuantity(assortment[4]);
// shoppingCart.decreaseQuantity(assortment[4]);
// shoppingCart.decreaseQuantity(assortment[4]);
// shoppingCart.decreaseQuantity(assortment[4]);
// shoppingCart.addItem(assortment[3]);
// shoppingCart.addItem(assortment[3]);
// shoppingCart.addItem(assortment[4]);
// shoppingCart.addItem(assortment[4]);
// shoppingCart.increaseQuantity(assortment[4]);
// shoppingCart.increaseQuantity(assortment[4]);
// shoppingCart.increaseQuantity(assortment[4]);
// shoppingCart.getItems();
// console.table(assortment);
// shoppingCart.decreaseQuantity(assortment[4]);
// shoppingCart.decreaseQuantity(assortment[4]);
// shoppingCart.decreaseQuantity(assortment[4]);
// shoppingCart.getItems();}
*/
// shoppingCardWithDestr calls

// console.log(shoppingCardWithDestr.getItems());
// shoppingCardWithDestr.addItem(assortment[0]);
// shoppingCardWithDestr.addItem(assortment[0]);
// shoppingCardWithDestr.addItem(assortment[1]);
// shoppingCardWithDestr.addItem(assortment[1]);
// shoppingCardWithDestr.addItem(assortment[2]);
// shoppingCardWithDestr.addItem(assortment[3]);
// shoppingCardWithDestr.addItem(assortment[3]);
// shoppingCardWithDestr.addItem(assortment[3]);
// console.table(shoppingCardWithDestr.getItems());
// shoppingCardWithDestr.removeItem(assortment[0]);
// shoppingCardWithDestr.removeItem(assortment[3]);
// console.table(shoppingCardWithDestr.getItems());
// shoppingCardWithDestr.decreaseQuantity(assortment[3]);
// shoppingCardWithDestr.decreaseQuantity(assortment[3]);
// shoppingCardWithDestr.decreaseQuantity(assortment[3]);
// console.table(shoppingCardWithDestr.getItems());
// shoppingCardWithDestr.increaseQuantity(assortment[3]);
// shoppingCardWithDestr.increaseQuantity(assortment[3]);
// shoppingCardWithDestr.increaseQuantity(assortment[3]);
// console.table(shoppingCardWithDestr.getItems());
// console.log(shoppingCardWithDestr.countTotalPrice());
function slugify(title) {
  return title.toLowerCase().split(' ').join('-');
}
console.log(slugify('Arrays for beginners'));

const findLongestWord = (string) => {
  let longestWord = '';
  for (const word of string.split(' ')) {
    if (word.length > longestWord.length) {
      longestWord = word;
    }
  }
  console.log(longestWord);
};

findLongestWord('The quick brown fox jumped over the lazy dog');

// const age = 18;
// const name = 'Dima';

const obj = { age: 18, name: 'Dima' };

const obj2 = Object.assign(obj, { male: 'men' });

console.log('obj-2= ', obj2);
