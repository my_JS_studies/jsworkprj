'use strict';
import * as part_01 from './js/part_01.js';
import * as part_02 from './js/part-02.js';
import * as indexOfStudy from './js/idxStudy.js';
import * as objFilterStudy from './js/jsFilterStudy.js';
import * as arrayReduceStudy from './js/arrayReduceStudy.js';
// const arrOfTenEl = [10, 1, 5, 9, 15, 21, 68, 90, 2, 3];
// const arrOfFiveEl = [5, 20, 6, 9, 4];
// const drinks = [
//   { name: 'vine', amount: 50 },
//   { name: 'beer', amount: 100 },
//   { name: 'champaign', amount: 30 },
//   { name: 'cocktail', amount: 120 },
// ];

// console.log(part_01.filter(arrOfTenEl, (e) => e >= 10));
// console.log(part_01.filter(arrOfFiveEl, (e) => e <= 6));
// console.log(part_01.filter(drinks, (el) => el.amount >= 100));

// const mangoCook = part_01.makeCook('Mango');
// console.dir(mangoCook);
// mangoCook('Meat balls');

// part_01.makeCook('Dima')('Potato');

// const objA = {
//   a: 10,
//   b: {
//     c: 20,
//     d: 30,
//   },
// };

// const copy = {
//   ...objA,
// };
// console.log(objA);
// console.log(copy);
// console.log(objA === copy);
// console.log(objA.b === copy.b);

// console.log(typeof objA);
