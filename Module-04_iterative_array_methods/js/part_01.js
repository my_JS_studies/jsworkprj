'use strict';

export { filter, makeCook };

//callback functions

const filter = function (arr, cb) {
  const filteredArr = [];
  for (const el of arr) {
    if (cb(el)) filteredArr.push(el);
  }
  return filteredArr;
};

//closure in functions

const makeCook = (name) =>
  function (dish) {
    console.log(`The cook ${name} is cooking a ${dish}`);
  };
