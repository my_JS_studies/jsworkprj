'use strict';

const src = '/team'; //from somewhere

const linksRef = document.querySelector(`.navigation-list__link[href="${src}"]`).parentElement;

export const currentPageHighlighter = () => {
  linksRef.classList.add('navigation-list__item__current');
  console.log(linksRef);
};
