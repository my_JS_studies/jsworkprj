'use strict';
const markupMaker = () => {
  const article = document.createElement('article');
  article.classList.add('article');

  const articleHeading = document.createElement('h2');
  articleHeading.classList.add('article-heading');
  articleHeading.textContent = 'News';

  const newsList = document.createElement('ul');
  newsList.classList.add('news-list');

  article.prepend(articleHeading, newsList);

  return article;
};

export const makeMarkup = () => {
  const containerRef = document.querySelector('.container');
  containerRef.append(markupMaker());
};
