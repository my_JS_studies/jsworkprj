export default [
  {
    author: 'KELVIN CHAN AP business writer',
    title: "Tesla's Swedish labor dispute pits anti-union Musk against Scandinavian worker ideals",
    description:
      'Tesla has found itself locked in an increasingly bitter dispute with union workers in Sweden and neighboring countries',
    url: 'https://abcnews.go.com/Business/wireStory/teslas-swedish-labor-dispute-pits-anti-union-musk-105799940',
    urlToImage:
      'https://i.abcnewsfe.com/a/c485bf31-f2ff-4cb1-9d56-9c8503205933/wirestory_5124a0074f6b4c5b7f503b5353afe8b3_16x9.jpg?w=992',
    publishedAt: '2023-12-20T07:50:29Z',
    content:
      "Tesla has found itself locked in an increasingly bitter dispute with union workers in Sweden and neighboring countries. The showdown pits the electric car maker's CEO Elon Musk, whos staunchly anti-u… [+6600 chars]",
  },
  {
    author: 'apnews.com',
    title: "Tesla's Swedish labor dispute pits anti-union Musk against Scandinavian worker ideals",
    description:
      "Tesla has found itself locked in an increasingly bitter dispute with union workers in Sweden and neighboring countries. The showdown pits the electric car maker's CEO Elon Musk, who’s staunchly anti-union, against the strongly held labor ideals of Scandinavia…",
    url: 'https://biztoc.com/x/e525162d39c2cadc',
    urlToImage: 'https://c.biztoc.com/p/e525162d39c2cadc/s.webp',
    publishedAt: '2023-12-20T07:40:38Z',
    content:
      "Tesla has found itself locked in an increasingly bitter dispute with union workers in Sweden and neighboring countries. The showdown pits the electric car maker's CEO Elon Musk, whos staunchly anti-u… [+286 chars]",
  },
  {
    author: 'cnbc.com',
    title:
      'Tesla cut EV prices in China more than BYD did for its flagship Han sedan this year, study finds',
    description:
      "BYD's Han electric car, pictured here at the 2021 Shanghai auto show, is one of the most popular new energy vehicles in China. BEIJING — Tesla cut prices for its electric cars in China by more than BYD did for its flagship Han sedan, according to analysis Wed…",
    url: 'https://biztoc.com/x/89a9c161c3cd2ab6',
    urlToImage: 'https://c.biztoc.com/p/89a9c161c3cd2ab6/s.webp',
    publishedAt: '2023-12-20T07:36:06Z',
    content:
      "BYD's Han electric car, pictured here at the 2021 Shanghai auto show, is one of the most popular new energy vehicles in China.BEIJING Tesla cut prices for its electric cars in China by more than BYD … [+271 chars]",
  },
  {
    author: null,
    title: 'Volkswagen, Audi, Porsche to Boost Charging Network by Adopting Tesla Port',
    description:
      "(marketscreener.com) \n By David Sachs \n\n\n Four brands under the Volkswagen Group umbrella will change their charging system to join Tesla's network in North America. \n\n Namesake brand Volkswagen, as well as Audi, Porsche and Scout Motors, said late Tuesday th…",
    url: 'https://www.marketscreener.com/quote/stock/VOLKSWAGEN-AG-436737/news/Volkswagen-Audi-Porsche-to-Boost-Charging-Network-by-Adopting-Tesla-Port-45605997/',
    urlToImage: 'https://www.marketscreener.com/images/twitter_MS_fdnoir.png',
    publishedAt: '2023-12-20T07:25:03Z',
    content:
      "By David Sachs \r\nFour brands under the Volkswagen Group umbrella will change their charging system to join Tesla's network in North America. \r\nNamesake brand Volkswagen, as well as Audi, Porsche and … [+946 chars]",
  },
  {
    author: 'Evelyn Cheng',
    title:
      'Tesla cut EV prices in China more than BYD did for its flagship Han sedan this year, study finds',
    description:
      'Tesla cut prices for its electric cars in China by more than BYD did for its flagship Han sedan, according to analysis from U.S.-based firm JL Warren Capital.',
    url: 'https://www.cnbc.com/2023/12/20/tesla-cut-china-ev-prices-more-than-byd-did-for-its-flagship-han-sedan.html',
    urlToImage:
      'https://image.cnbcfm.com/api/v1/image/106871420-1618991336529-Image_from_iOS_17.jpg?v=1681147205&w=1920&h=1080',
    publishedAt: '2023-12-20T07:19:28Z',
    content:
      "BYD's Han electric car, pictured here at the 2021 Shanghai auto show, is one of the most popular new energy vehicles in China.\r\nBEIJING Tesla cut prices for its electric cars in China by more than BY… [+2282 chars]",
  },
  {
    author: 'Charlie Martin',
    title: 'Winners and losers 2023: New car market round-up',
    description:
      "The UK's new car market is slowly recovering, but some brands came out better – or worse – than the pack\n\nThe latest data from the UK car industry has revealed that Lexus now has a bigger market share than Jaguar, Cupra is on course to overtake its older sibl…",
    url: 'https://www.autocar.co.uk/car-news/new-cars/winners-and-losers-2023',
    urlToImage:
      'https://www.autocar.co.uk/sites/autocar.co.uk/files/images/car-reviews/first-drives/legacy/winners-and-losers-2023.jpg',
    publishedAt: '2023-12-20T07:01:46Z',
    content:
      'Meanwhile, Seat sales increased by 42.5%, to 28,741. That puts its market share at 1.63% and Cupra well within touching distance for 2024.\r\nThe launch of the Cupra Tavascan as well as facelifts for t… [+1669 chars]',
  },
  {
    author: null,
    title: 'Nearly half of new passenger cars in EU electrified -ACEA',
    description:
      '(marketscreener.com) Sales of electric cars in the\nEuropean Union were almost half of all new passenger car\nregistrations in the EU between January and November 2023 and\nalready crossed the halfway mark in the month of November alone,\ndata showed on Wednesday…',
    url: 'https://www.marketscreener.com/quote/stock/VOLKSWAGEN-AG-436737/news/Nearly-half-of-new-passenger-cars-in-EU-electrified-ACEA-45604922/',
    urlToImage:
      'https://www.marketscreener.com/images/reuters/2023-12/2023-12-20T070341Z_1_LYNXMPEJBJ061_RTROPTP_3_EUROPE-VEHICLEREGISTRATIONS.JPG',
    publishedAt: '2023-12-20T07:00:00Z',
    content:
      'Dec 20 (Reuters) - Sales of electric cars in the\r\nEuropean Union were almost half of all new passenger car\r\nregistrations in the EU between January and November 2023 and\r\nalready crossed the halfway … [+2410 chars]',
  },
  {
    author: 'Joseph Grosso',
    title: 'A New Hope: The UAW Victory and Beyond',
    description:
      'If there has been a constant theme to Left discourse in the U.S. it has been that a labor revival is always just around the corner. While this has proved to be quite elusive, it can be said that this year has put workers rights squarely in the spotlight. Ther…',
    url: 'https://www.counterpunch.org/2023/12/20/a-new-hope-the-uaw-victory-and-beyond-2/',
    urlToImage:
      'https://www.counterpunch.org/wp-content/uploads/2023/12/Screen-Shot-2023-12-19-at-11.31.01-AM.png',
    publishedAt: '2023-12-20T06:55:17Z',
    content:
      'Image courtesy of UAW.\r\nIf there has been a constant theme to Left discourse in the U.S. it has been that a labor revival is always just around the corner. While this has proved to be quite elusive, … [+8077 chars]',
  },
  {
    author: null,
    title: 'Wall Street: records of all kinds pile up inexorably',
    description:
      '(marketscreener.com) The race for absolute records seems unstoppable on Wall Street, with a new shower of records: the Dow Jones ended on a high note, up +0.68% to 37,558, as did the S&P500, which gained +0.59% to 4,768 , while the Nasdaq Composite broke thro…',
    url: 'https://www.marketscreener.com/quote/index/DOW-JONES-INDUSTRIAL-4945/news/Wall-Street-records-of-all-kinds-pile-up-inexorably-45604807/',
    urlToImage:
      'https://www.marketscreener.com/images/reuters/2016-09-08T200430Z_1_LYNXNPEC871FV_RTROPTP_2_MARKETS-STOCKS.JPG',
    publishedAt: '2023-12-20T06:40:05Z',
    content:
      'The race for absolute records seems unstoppable on Wall Street, with a new shower of records: the Dow Jones ended on a high note, up +0.68% to 37,558, as did the S&amp;P500, which gained +0.59% to 4,… [+2106 chars]',
  },
  {
    author: 'Admin',
    title: 'Tesla Moves to Pause EEOC Lawsuit Alleging Race Bias at Factory',
    description:
      'Tesla Inc has asked a U.S. judge to pause a federal agency’s lawsuit accusing the electric carmaker of severe harassment of Black workers at its California assembly plant, saying two similar cases should play out first. Tesla, in a filing …',
    url: 'https://www.insurancejournal.com/news/national/2023/12/20/752703.htm',
    urlToImage:
      'https://www.insurancejournal.com/app/uploads/2022/07/Tesla-inside-Bigstock-scaled.jpg',
    publishedAt: '2023-12-20T06:13:27Z',
    content:
      'Tesla Inc has asked a U.S. judge to pause a federal agency’s lawsuit accusing the electric carmaker of severe harassment of Black workers at its California assembly plant, saying two similar cases sh… [+1943 chars]',
  },
];
