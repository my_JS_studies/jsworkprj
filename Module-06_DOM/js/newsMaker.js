'use strict';
import newsResponse from './newsResponse.js';

const newsMaker = ({ title, description, url, urlToImage }) => {
  const newsListItem = document.createElement('li');
  newsListItem.classList.add('news-list__item');

  const articleHeading = document.createElement('h3');
  articleHeading.classList.add('article-heading');
  articleHeading.textContent = title;

  const articleDescription = document.createElement('p');
  articleDescription.classList.add('news-list__item-description');
  articleDescription.textContent = description;

  const articleImg = document.createElement('img');
  articleImg.src = urlToImage;

  const articleLink = document.createElement('a');
  articleLink.classList.add('news-list__item-link');
  articleLink.href = url;
  articleLink.textContent = 'Link to the origin';

  newsListItem.prepend(articleHeading, articleDescription, articleImg, articleLink);

  return newsListItem;
};

export const makeNews = () => {
  const newsListRef = document.querySelector('.news-list');
  newsListRef.append(...newsResponse.map(newsMaker));
};
