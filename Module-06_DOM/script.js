import { makeMarkup } from './js/markupMaker.js';
import { makeNews } from './js/newsMaker.js';

import { currentPageHighlighter } from './js/currentPageHighlighter.js';

const markupBtnRef = document.querySelector('button[data-action="make-markup"]');
const newsBtnRef = document.querySelector('button[data-action="add-news"]');

markupBtnRef.addEventListener('click', makeMarkup);
newsBtnRef.addEventListener('click', makeNews);

currentPageHighlighter();
